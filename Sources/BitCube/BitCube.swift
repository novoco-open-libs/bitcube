/**
 BitCube represents the triple relationships between indexed values as a three dimensional cube of bit values
 The index values are local to this BitCube and have no direct mapping to the index values fo any other cube.
 BitCube supports fast diff between revisions of the same cube as well as fast traveral of the tripe graph structure.
 */

import CoreFoundation
import SwiftRoaring

public enum BitCubeError: Error {
    case initialization(bits: UInt64)
}

/*
public protocol BitC: Sequence {
    //associatedtype Element = (x: UInt64, y: UInt64, z: UInt64)
    //associatedtype Element where Self.Element == Self.Iterator.Element
    
    associatedtype Element
    
    associatedtype Iterator = IndexingIterator<Self>
    
    override func makeIterator() -> Self.Iterator
    
    var count: Int { get }
    subscript(x: UInt64, y: UInt64, z: UInt64) -> Bool { get set }
    
    func getBitsWithYZ(y: UInt32, z: UInt32) -> BitC
    func getBitsWithZX(z: UInt32, x: UInt32) -> BitC
    func getBitsWithXY(x: UInt32, y: UInt32) -> BitC
    func getBitsWithX(x: UInt32) -> BitC
    func getBitsWithY(y: UInt32) -> BitC
    func getBitsWithZ(z: UInt32) -> BitC
}
 */

public class BitCube: Sequence {
    
    public typealias Element = (x: UInt64, y: UInt64, z: UInt64)
    var bitStore = RoaringBitmap()
    let m: Morton
    let maxBits = 10 //RoaringBitmap is currently limited to 32bit values, 3 dimensions max to bits each
    
    public init(bits: UInt64) throws {
        guard bits <= maxBits else {
            throw BitCubeError.initialization(bits: bits)
        }
        
        m = try! Morton(dimensions: 3, bits: bits)
    }
    
    public var count: Int {
        return Int(bitStore.count)
    }
    
    public subscript(x: UInt64, y: UInt64, z: UInt64) -> Bool {
        get {
            //TODO: subscripts cant throw an error on out of bounds indeces
            //so return false in this case
            let code = try! m.pack(x, y, z)
            return bitStore.contains(UInt32(code))
        }
        set (newValue) {
            //TODO: subscripts cant throw an error on out of bounds indeces
            //so just drop the request in this case
            let code = try! m.pack(x, y, z)
            if newValue {
                bitStore.add(UInt32(code))
            } else {
                bitStore.remove(UInt32(code))
            }
        }
    }
    
    public func getBitsWithYZ(y: UInt32, z: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[1] == UInt64(y) && v[2] == UInt64(z) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func getBitsWithZX(z: UInt32, x: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[2] == UInt64(z) && v[0] == UInt64(x) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func getBitsWithXY(x: UInt32, y: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[0] == UInt64(x) && v[1] == UInt64(y) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func getBitsWithX(x: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[0] == UInt64(x) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func getBitsWithY(y: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[1] == UInt64(y) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func getBitsWithZ(z: UInt32) -> BitCube {
        let resultCube = try! BitCube(bits: m.bits)
        for bit in bitStore {
            let v = m.unpack(Int64(bit))
            if v[2] == UInt64(z) {
                resultCube[v[0],v[1],v[2]] = true
            }
        }
        return resultCube
    }
    
    public func equals(_ other: BitCube) -> Bool {
        return bitStore.equals(other.bitStore)
    }
    
    /**
     Return iterator over complete bitcube
     */
    public func makeIterator() -> BitCubeIterator {
        return BitCubeIterator(rbm: bitStore, morton: m)
    }

    /**
     For each set bit return the X,Y,Z values
     */
    public struct BitCubeIterator: IteratorProtocol {
        
        public typealias Element = (x: UInt64, y: UInt64, z: UInt64)
        
        private var i: RoaringBitmap.RoaringBitmapIterator
        private let mn: Morton
        
        init(rbm: RoaringBitmap, morton: Morton) {
            self.i = rbm.makeIterator()
            self.mn = morton
        }
        
        mutating public func next() -> Element? {
            if let n = i.next() {
                let values = mn.unpack(Int64(n))
                let bt = Element(values[0],values[1],values[2])
                return bt
            } else {
                return nil
            }
        }
    }
}

