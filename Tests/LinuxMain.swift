import XCTest

import BitCubeTests

var tests = [XCTestCaseEntry]()
tests += BitCubeTests.allTests()
XCTMain(tests)
