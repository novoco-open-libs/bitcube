import Foundation
import Quick
import Nimble

import BitCube

final class BitCubeTests: QuickSpec {
    override func spec() {
        
        func doTestInitialize(_ bits: UInt64) {
            it("can't initialize bits \(bits)") {
                expect {
                    try BitCube(bits: bits)
                }.to(throwError())
            }
        }
        
        func doTestSetGet(_ bits: UInt64, _ values: [UInt64]) {
            it("set/get \(values) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                bc[values[0], values[1], values[2]] = true
                let b = bc[values[0], values[1], values[2]]
                expect(b).to(equal(true))
            }
        }
        
        /**
         Test iterator, where inserted values are added in ascending morton order
         */
        func doTestIterateAscendingOrder(_ bits: UInt64, _ values: [[UInt64]]) {
            it("iterate \(values) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v[0], v[1], v[2]] = true
                }
                //let b = bc[values[0], values[1], values[2]]
                var result: [[UInt64]] = []
                for e in bc {
                    result.append([ e.x, e.y, e.z])
                }
                expect(result).to(equal(values))
            }
        }
        
        /**
         Test iterator, where inserted values are in radom morton order, but iterator is in ascending morton order
         */
        func doTestIterateOutOfOrder(_ bits: UInt64, _ values: [[UInt64]], _ expected: [[UInt64]]) {
            it("iterate \(values) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v[0], v[1], v[2]] = true
                }
                //let b = bc[values[0], values[1], values[2]]
                var result: [[UInt64]] = []
                for e in bc {
                    result.append([ e.x, e.y, e.z])
                }
                expect(result).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithX(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ xvalue: UInt32, _ expected: Int) {
            it("get bits with X \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithX(x: xvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithY(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ yvalue: UInt32, _ expected: Int) {
            it("get bits with Y \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithY(y: yvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithZ(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ zvalue: UInt32, _ expected: Int) {
            it("get bits with Z \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithZ(z: zvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithYZ(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ yvalue: UInt32, _ zvalue: UInt32, _ expected: Int) {
        
            it("get bits with YZ \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithYZ(y: yvalue, z: zvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithZX(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ zvalue: UInt32, _ xvalue: UInt32, _ expected: Int) {
        
            it("get bits with ZX \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithZX(z: zvalue, x: xvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestGetBitsWithXY(_ bits: UInt64, _ values: [(x: UInt64, y: UInt64, z: UInt64)], _ xvalue: UInt32, _ yvalue: UInt32, _ expected: Int) {
        
            it("get bits with ZX \(values) expected \(expected) with \(bits) bits") {
                let bc: BitCube = try! BitCube(bits: bits)
                for v in values {
                    bc[v.x, v.y, v.z] = true
                }

                let result = bc.getBitsWithXY(x: xvalue, y: yvalue)
                expect(result.count).to(equal(expected))
            }
        }
        
        func doTestEquals(_ bits: UInt64, _ lhsvalues: [(x: UInt64, y: UInt64, z: UInt64)], _ rhsvalues: [(x: UInt64, y: UInt64, z: UInt64)]) {
            it("equals with \(bits) bits") {
                let lhs: BitCube = try! BitCube(bits: bits)
                for v in lhsvalues {
                    lhs[v.x, v.y, v.z] = true
                }
                
                let rhs: BitCube = try! BitCube(bits: bits)
                for v in rhsvalues {
                    rhs[v.x, v.y, v.z] = true
                }
                
                expect(lhs.equals(rhs)).to(equal(true))
                expect(rhs.equals(lhs)).to(equal(true))
            }
        }
        
        
        describe("initialization") {
            doTestInitialize(11)
        }
        
        //BitCube uses subscript for setting and getting and can not throw errors if the index is
        //out of range. Waiting for language support for Throw in subscript
        /*
        describe("value boundaries") {
            doTestValueBoundaries(2, 1, 2)
            doTestValueBoundaries(16, 4, 16)
        }
        */
        
        describe("set/get") {
            doTestSetGet(1, [0, 1, 0])
            doTestSetGet(1, [1, 1, 1])
            doTestSetGet(2, [0, 1, 2])
            doTestSetGet(2, [1, 2, 3])
            doTestSetGet(2, [3, 3, 3])
            doTestSetGet(3, [4, 6, 7])
            doTestSetGet(3, [7, 7, 7])
            doTestSetGet(4, [7, 0, 12])
            doTestSetGet(4, [7, 8, 14])
            doTestSetGet(4, [15, 15, 15])
            doTestSetGet(5, [31, 31, 31])
            doTestSetGet(6, [63, 63, 63])
            doTestSetGet(7, [127, 127, 127])
            doTestSetGet(8, [255, 255, 255])
            doTestSetGet(9, [511, 511, 511])
            doTestSetGet(10, [1023, 1023, 1023])
        }
        
        describe("iterateInOrder") {
            doTestIterateAscendingOrder(1, [[0, 0, 0]])
            doTestIterateAscendingOrder(1, [[0, 1, 0]])
            doTestIterateAscendingOrder(5, [[0, 1, 0],[1, 1, 0],[10, 1, 2]])
            doTestIterateAscendingOrder(5, [[1, 1, 0],[10, 1, 2],[10,10,10]])
        }
        
        describe("iterateOutOfOrder") {
            doTestIterateOutOfOrder(1, [[0, 1, 0]],[[0, 1, 0]])
            doTestIterateOutOfOrder(5, [[0, 1, 0],[1, 1, 0],[10, 1, 2]], [[0, 1, 0],[1, 1, 0],[10, 1, 2]])
            doTestIterateOutOfOrder(5, [[10, 1, 2],[10,10,10],[1, 1, 0]], [[1, 1, 0],[10, 1, 2],[10,10,10]])
        }
        
        describe("getBitsWithX") {
            doTestGetBitsWithX(10, [(x:0,y:0,z:0)], 0, 1)
            doTestGetBitsWithX(10, [(x:1,y:2,z:3)], 1, 1)
            doTestGetBitsWithX(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 1, 2)
            doTestGetBitsWithX(10, [(x:1,y:2,z:3),(x:1,y:2,z:4)], 1, 2)
            doTestGetBitsWithX(10, [(x:1,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7)], 1, 3)
            doTestGetBitsWithX(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:6,z:1)], 1, 3)
        }
        
        describe("getBitsWithY") {
            doTestGetBitsWithY(10, [(x:0,y:0,z:0)], 0, 1)
            doTestGetBitsWithY(10, [(x:1,y:2,z:3)], 1, 0)
            doTestGetBitsWithY(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 2, 1)
            doTestGetBitsWithY(10, [(x:1,y:2,z:3),(x:1,y:2,z:4)], 2, 2)
            doTestGetBitsWithY(10, [(x:1,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7)], 4, 1)
            doTestGetBitsWithY(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:6,z:1)], 2, 2)
        }
        
        describe("getBitsWithZ") {
            doTestGetBitsWithZ(10, [(x:0,y:0,z:0)], 0, 1)
            doTestGetBitsWithZ(10, [(x:1,y:2,z:3)], 1, 0)
            doTestGetBitsWithZ(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 3, 1)
            doTestGetBitsWithZ(10, [(x:1,y:2,z:3),(x:1,y:4,z:3)], 3, 2)
            doTestGetBitsWithZ(10, [(x:1,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7)], 7, 1)
            doTestGetBitsWithZ(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:6,z:1)], 3, 2)
        }
        
        describe("getBitsWithYZ") {
            doTestGetBitsWithYZ(10, [(x:0,y:0,z:0)], 0, 0, 1)
            doTestGetBitsWithYZ(10, [(x:0,y:0,z:0)], 0, 10, 0)
            doTestGetBitsWithYZ(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 2, 3, 1)
            doTestGetBitsWithYZ(10, [(x:1,y:2,z:3),(x:4,y:2,z:3)], 2, 3, 2)
            doTestGetBitsWithYZ(10, [(x:1,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7)], 6, 7, 1)
            doTestGetBitsWithYZ(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:2,z:3)], 2, 3, 2) //duplicate triple ignored
            doTestGetBitsWithYZ(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:8,y:2,z:3)], 2, 3, 3)
        }
        
        describe("getBitsWithZX") {
            doTestGetBitsWithZX(10, [(x:0,y:0,z:0)], 0, 0, 1)
            doTestGetBitsWithZX(10, [(x:0,y:0,z:0)], 0, 10, 0)
            doTestGetBitsWithZX(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 3, 1, 1)
            doTestGetBitsWithZX(10, [(x:1,y:2,z:3),(x:1,y:4,z:3)], 3, 1, 2)
            doTestGetBitsWithZX(10, [(x:1,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7)], 5, 1, 1)
            doTestGetBitsWithZX(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:2,z:3)], 3, 5, 1) //duplicate triple ignored
            doTestGetBitsWithZX(10, [(x:1,y:2,z:3),(x:1,y:4,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:1,y:8,z:3)], 3, 1, 3)
        }
        
        describe("getBitsWithXY") {
            doTestGetBitsWithXY(10, [(x:0,y:0,z:0)], 0, 0, 1)
            doTestGetBitsWithXY(10, [(x:0,y:0,z:0)], 0, 10, 0)
            doTestGetBitsWithXY(10, [(x:1,y:2,z:3),(x:1,y:4,z:5)], 1, 2, 1)
            doTestGetBitsWithXY(10, [(x:1,y:2,z:3),(x:1,y:2,z:4)], 1, 2, 2)
            doTestGetBitsWithXY(10, [(x:1,y:2,z:3),(x:1,y:5,z:4),(x:1,y:5,z:7)], 1, 5, 2)
            doTestGetBitsWithXY(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:2,z:3)], 5, 2, 1) //duplicate triple ignored
            doTestGetBitsWithXY(10, [(x:1,y:2,z:3),(x:5,y:2,z:3),(x:1,y:4,z:5),(x:1,y:6,z:7),(x:5,y:2,z:8)], 5, 2, 2)
        }
        
        describe("equals") {
            doTestEquals(10, [(0, 0, 0)],[(0, 0, 0)])
            doTestEquals(10, [(0, 1, 0)],[(0, 1, 0)])
            doTestEquals(10, [(0, 1, 0),(10, 1, 10)],[(10, 1, 10),(0, 1, 0)])
            doTestEquals(10, [(0, 1, 0),(10, 1, 10),(20, 21, 22)],[(20, 21, 22),(10, 1, 10),(0, 1, 0)])
        }
    }
}

